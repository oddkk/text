struct paged_arena {
	static constexpr size_t arena_page_size = 1024;
	static constexpr size_t arena_page_min_fragment = 16;

	struct arena_page {
		uint8_t data[arena_page_size];
		size_t used;
		size_t users;
	};

	std::list<arena_page> pages;

	using arena_page_iterator = std::list<arena_page>::iterator;

	std::pair<arena_page*, string> alloc_part_str(size_t target) {
		// TODO: Improve this algorithm.
		arena_page_iterator best_fit = pages.end();
		size_t best_fit_remaining = 0;

		for (auto iter = pages.begin(); iter != pages.end(); iter++) {
			assert(iter->used <= arena_page_size);

			size_t remaining = arena_page_size - iter->used;

			if (remaining >= target) {
				best_fit = iter;
				best_fit_remaining = remaining;
				break;
			}

			if (remaining > arena_page_min_fragment && remaining > best_fit_remaining) {
				best_fit = iter;
				best_fit_remaining = remaining;
			}
		}

		if (best_fit == pages.end()) {
			pages.emplace_front();
			best_fit = pages.begin();
			best_fit_remaining = arena_page_size;
		}

		string result = {};

		result.text = &best_fit->data[best_fit->used];
		result.length = std::min(target, best_fit_remaining);
		best_fit->used += result.length;
		best_fit->users += 1;

		return std::make_pair(&*best_fit, result);
	}

	void remove_page_user(arena_page *page) {
		if (page) {
			assert(page->users > 0);
			page->users -= 1;
		}
		if (page->users == 0) {
			page->used = 0;
		}
	}
};
