struct file_buffer {
	using size_t page_size = 1024;
	using size_t num_pages = 100;
	struct page_header {
		bool used:1;
		bool modified:1;
		size_t position;
	};
	page_header *headers;
	uint8_t *data;
	string file;

	file_buffer(string file) {
		this->file = file;
		this->headers = calloc(num_pages, sizeof(page_header));
		this->data = calloc(num_pages, page_size);
	}

	size_t get_free_page() {
		for (size_t i = 0; i < num_pages; ++i) {
		}
	}
};
