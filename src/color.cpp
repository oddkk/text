struct color {
	color() = default;
	color(uint8_t r, uint8_t g, uint8_t b, uint8_t a=255)
		: r(r), g(g), b(b), a(a) {}

	uint8_t r, g, b, a;

	uint32_t value() const {
		uint32_t res = 0;

		res = (r << 24) | (g << 16) | (b << 8) | (a << 0);

		return res;
	}
};

static inline struct color rgb(uint8_t r, uint8_t g, uint8_t b) {
	color result;
	result.r = r;
	result.g = g;
	result.b = b;
	return result;
}
