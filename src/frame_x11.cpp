#include <X11/Xlib.h>
#include <X11/keysym.h>
#include "x11_keycodes.cpp"

struct x11_context {
	messaging::messages messages;
	cairo_draw_context draw_ctx;
	cairo_surface_t *surface;
	Atom wm_delete_window;
	bool should_close;
	Display *display;
	Visual *visual;
	Window window;
	frame *frame;
};

x11_keycode *x11_lookup_keysym(KeySym sym) {
	size_t l = 0, r = keycode_to_unicode_len;

	while (r - l > 1) {
		size_t m = l + (r - l) / 2;
		if (keycode_to_unicode[m].sym <= sym) {
			l = m;
		} else {
			r = m;
		}
	}

	if (keycode_to_unicode[l].sym == sym) {
		return &keycode_to_unicode[l];
	} else {
		return &keycode_to_unicode[keycode_to_unicode_len - 1];
	}
}

void
frame_x11_dispatch_event(x11_context *ctx, XEvent event) {
	switch (event.type) {
	case ClientMessage: {
		if ((Atom)event.xclient.data.l[0] == ctx->wm_delete_window) {
			ctx->should_close = true;
		}
	} break;

	case Expose: {
		rectangle region = {
			event.xexpose.x,
			event.xexpose.y,
			event.xexpose.width,
			event.xexpose.height
		};
		expose_damaged_frame(&ctx->draw_ctx, ctx->frame, region);
	} break;

	case ConfigureNotify: {
		ctx->frame->resize(event.xconfigure.width,
						   event.xconfigure.height);
		cairo_xlib_surface_set_size(ctx->surface,
									event.xconfigure.width,
									event.xconfigure.height);
		messaging::message msg;
		msg.type = messaging::message_type::geometry_changed;
		msg.geometry_changed.new_width = event.xconfigure.width;
		msg.geometry_changed.new_height = event.xconfigure.height;
		ctx->messages.send(msg);
	} break;

	case KeyPress: {
		KeySym keysym;
		keysym = XLookupKeysym(&event.xkey, 0);
		messaging::message msg;
		msg.type = messaging::message_type::keypress;
		msg.keypress.keysym = keysym;
		ctx->messages.send(msg);
		expose_frame(&ctx->draw_ctx, ctx->frame);
	} break;

	case KeyRelease: {} break;

	}
}

void frame_x11_dispatch_message(x11_context *ctx, messaging::message msg) {
	switch (msg.type) {
	case messaging::message_type::create_frame_response: {
		ctx->frame->id = msg.create_frame_response.frame_id;
	} break;

	default: {
		if ((size_t)msg.type < sizeof(messaging::message_type_name) / sizeof(string)) {
			fprintf(stderr, "client: Got unexpected message with id %i (%.*s)\n", (int)msg.type, LIT(messaging::message_type_name[(int)msg.type]));
		} else {
			fprintf(stderr, "client: Got unexpected message with id %i\n", (int)msg.type);
		}
	} break;
	}
}

int frame_x11_open_window(x11_context *ctx) {
	XSetWindowAttributes window_attrs = {};
	Window root;
	int screen;

	window_attrs.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | StructureNotifyMask;

	screen = DefaultScreen(ctx->display);
	ctx->visual = DefaultVisual(ctx->display, screen);
	root = XRootWindow(ctx->display, screen);

	ctx->window = XCreateWindow(
		ctx->display, root,
		0, 0, 800, 600, 0,
		CopyFromParent,
		InputOutput,
		ctx->visual,
		CWEventMask, &window_attrs
	);

	XMapWindow(ctx->display, ctx->window);
	XSetWMProtocols(ctx->display, ctx->window, &ctx->wm_delete_window, 1);

	return 0;
}

int frame_x11(frame *frame) {
	// messaging::messages messages;
	//bool should_close = false;
	//Display *display;
	//Window window;
	//Atom wm_delete_window;

	//cairo_surface_t *surface;


	x11_context ctx = {};

	ctx.frame = frame;
	ctx.display = XOpenDisplay(0);
	ctx.wm_delete_window = XInternAtom(ctx.display, "WM_DELETE_WINDOW", False);

	frame_x11_open_window(&ctx);

	ctx.surface = cairo_xlib_surface_create(
		ctx.display,
		ctx.window,
		ctx.visual,
		800, 600);
	cairo_xlib_surface_set_size(ctx.surface, 800, 600);
	ctx.draw_ctx.cairo = cairo_create(ctx.surface);

	ctx.messages.socket = frame->socket;

	{
		messaging::message connect_message;
		connect_message.type = messaging::message_type::create_frame_request;
		connect_message.create_frame_request.frontend = frame_frontend::X11;
		ctx.messages.send(connect_message);
	}

	fd_set in_fds;
	int x_fd = ConnectionNumber(ctx.display);

	while (!ctx.should_close && frame->window_heap.size() > 0) {
		int fd;

		FD_ZERO(&in_fds);
		FD_SET(x_fd, &in_fds);
		FD_SET(ctx.messages.socket, &in_fds);

		fd = select(x_fd+1, &in_fds, 0, 0, 0);

		if (fd == -1) {
			perror("select");
		} else {
			ctx.messages.try_receive().bind([&ctx](messaging::message msg) {
					frame_x11_dispatch_message(&ctx, msg);
				});
			while (XPending(ctx.display)) {
				XEvent event;
				XNextEvent(ctx.display, &event);
				frame_x11_dispatch_event(&ctx, event);
			}
		}
	}

	{
		messaging::message destroy_message;
		destroy_message.type = messaging::message_type::destroy_frame;
		destroy_message.destroy_frame.frame_id = ctx.frame->id;
		ctx.messages.send(destroy_message);
	}

	cairo_destroy(ctx.draw_ctx.cairo);
	cairo_surface_destroy(ctx.surface);
	XDestroyWindow(ctx.display, ctx.window);
	XCloseDisplay(ctx.display);
	return 0;
}
