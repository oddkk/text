struct string {
	uint8_t *text;
	size_t length;
};

#define CSTR_TO_STR(s) string{(uint8_t*)s, sizeof(s)-1}
#define LIT(s) ((int)s.length), (s.text)
