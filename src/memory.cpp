void copy_memory(void *dest, const void *src, size_t size) {
	memcpy(dest, src, size);
}

template<typename T>
void copy_struct(T &dest, const T &src) {
	copy_memory((void*)&dest, (const void*)&src, sizeof(T));
}

void zero_memory(void *ptr, size_t size) {
	memset(ptr, 0, size);
}

template<typename T>
void zero_struct(T &dt) {
	zero_memory((void*)&dt, sizeof(T));
}
