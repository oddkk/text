struct cairo_draw_context {
	cairo_t *cairo;
	char *buffer;
	size_t buffer_size;
};


void set_cairo_color(cairo_t *cairo, struct color color) {
	cairo_set_source_rgb(cairo,
						 (double)color.r / 255.0,
						 (double)color.g / 255.0,
						 (double)color.b / 255.0);
}

struct draw_string_context {
	double x, y;
	double max_height_in_line;
	struct color buffer_background_color;
};

void draw_string(struct cairo_draw_context *ctx, struct draw_string_context *sctx, struct string str, struct face face, bool should_draw=true) {
	cairo_font_extents_t font_extents;
	cairo_text_extents_t text_extents;

	should_draw = true;

	if (str.length + 1 > ctx->buffer_size) {
		ctx->buffer_size = str.length + 1;
		ctx->buffer = (char*)realloc(ctx->buffer, ctx->buffer_size);
	}

	copy_memory(ctx->buffer, str.text, str.length);
	ctx->buffer[str.length] = 0;

	cairo_select_font_face(ctx->cairo, (char*)face.family.text,
						CAIRO_FONT_SLANT_NORMAL,
						CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(ctx->cairo, face.size);
	cairo_font_extents(ctx->cairo, &font_extents);

	cairo_text_extents(ctx->cairo, (char*)ctx->buffer, &text_extents);

	if (should_draw) {

		// Background color
		{
			double x = sctx->x + text_extents.x_bearing;
			double y = sctx->y + font_extents.height - font_extents.ascent;
			double w = text_extents.x_advance;
			double h = font_extents.height;
			set_cairo_color(ctx->cairo, face.background_color);
			cairo_rectangle(ctx->cairo, x, y, w, h);
			cairo_fill(ctx->cairo);
		}

		// Text
		set_cairo_color(ctx->cairo, face.foreground_color);
		cairo_move_to(ctx->cairo, sctx->x, sctx->y + font_extents.height);
		cairo_show_text(ctx->cairo, (char*)ctx->buffer);

		// Underline
		if ((face.decoration & TEXT_DECORATION_UNDERLINE) != 0) {
			double x = sctx->x;
			double y = sctx->y + font_extents.height + font_extents.descent - 0.5;
			cairo_set_line_width(ctx->cairo, 1.0);
			cairo_move_to(ctx->cairo, x, y);
			cairo_line_to(ctx->cairo, x + text_extents.x_advance, y);
			cairo_stroke(ctx->cairo);
		}

		// Strike through
		if ((face.decoration & TEXT_DECORATION_STRIKETHROUGH) != 0) {
			double x = sctx->x;
			double y = sctx->y + font_extents.height + font_extents.descent - (font_extents.ascent / 2.0) - 0.5;
			cairo_set_line_width(ctx->cairo, 1.0);
			cairo_move_to(ctx->cairo, x, y);
			cairo_line_to(ctx->cairo, x + text_extents.x_advance, y);
			cairo_stroke(ctx->cairo);
		}

	}

	sctx->x += text_extents.x_advance;
	sctx->y += text_extents.y_advance;

	if (font_extents.height > sctx->max_height_in_line) {
		sctx->max_height_in_line = font_extents.height;
	}
}

bool regions_overlap(text_region lhs, text_region rhs) {
	int lhs_left = lhs.begin,
		lhs_right = lhs.begin + lhs.length,
	    rhs_left = rhs.begin,
		rhs_right = rhs.begin + rhs.length;

	return
		value_in_range(lhs_left, rhs_left, rhs_right) ||
		value_in_range(rhs_left, lhs_left, lhs_right) ||
		value_in_range(lhs_right, rhs_left, rhs_right) ||
		value_in_range(rhs_right, lhs_left, lhs_right);
}

void paint_window_leaf(cairo_draw_context *ctx, window *window) {
	draw_string_context sctx = {};
	face_segment_context syntax {};
	string str = window->buffer->text;
	string res = {};
	size_t line = 0;
	bool  update_all = window->buffer->should_update_all(window->last_change_updated);
	buffer::change_regions_t update_regions;
	size_t num_update_regions = 0;
	size_t current_update_region = 0;

	window->buffer->regions_to_update(window->last_change_updated,
									  &update_regions, &num_update_regions);

	// if (num_update_regions == 0) {
	// 	return;
	// }

	res.text = window->buffer->text.text;
	res.length = 0;

	syntax = face_segment_context(
		window->buffer->face_segments,
		window->buffer->num_face_segments,
		window->cursor,
		window->buffer->default_face);

	sctx.buffer_background_color = syntax.default_face->background_color;

	cairo_save(ctx->cairo);

	cairo_translate(ctx->cairo, window->bounds.x+1, window->bounds.y+1);

	if (update_all) {
		// Border
		cairo_set_source_rgb(ctx->cairo, 1.0, 0.0, 0.0);
		cairo_set_line_width(ctx->cairo, 1.0);
		cairo_rectangle(ctx->cairo, -0.5, -0.5, window->bounds.width-1, window->bounds.height-1);
		cairo_stroke(ctx->cairo);
	}

	cairo_rectangle(ctx->cairo, 0, 0, window->bounds.width - 2, window->bounds.height - 2);
	set_cairo_color(ctx->cairo, sctx.buffer_background_color);
	cairo_clip(ctx->cairo);

	if (update_all) {
		cairo_paint(ctx->cairo);
	}

	size_t ch = 0;
	size_t face_segment_begin = 0;
	size_t current_face_segment = 0;

	size_t i = 0;
	while (i < str.length) {
		int32_t c = str.text[i];
		size_t nbytes = 1;
		bool flush = false;
		bool discard = false;
		bool linebreak = false;

		// Skip until we are inside what is visible on screen
		if (line < window->scroll) {
			if (c == '\n') {
				line += 1;
				res.text = &str.text[i];
				res.length = 0;
			}
			continue;
		}

		if (c == '\n') {
			flush = true;
			discard = true;
			linebreak = true;
		}

		if (c == '\t') {
			flush = true;
			discard = true;
		}

		// if (ch >= face_segment_begin + syntax.segments[current_face_segment].length) {
		// 	flush = true;
		// }

		if (flush) {
			// while (current_face_segment < syntax.segments.size() &&
			// 	   ch - 1 >= face_segment_begin + syntax.segments[current_face_segment].length) {
			// 	face_segment_begin += syntax.segments[current_face_segment].length;
			// 	current_face_segment += 1;
			// }

			face *f = 0;

			// if (current_face_segment < syntax.segments.size()) {
			// 	f = syntax.segments[current_face_segment].face;
			// }

			if (!f) {
				f = syntax.default_face;
			}

			// while (current_update_region < update_regions.size() &&
			// 	   (!value_in_range(i - 1,
			// 						update_regions[current_update_region].begin,
			// 						update_regions[current_update_region].begin + update_regions[current_update_region].length) ||
			// 		i - 1 < update_regions[current_update_region].begin)) {
			// 	current_update_region += 1;
			// }

			// if (current_update_region >= update_regions.size()) {
			// 	// There is nothing more to do.
			// 	break;
			// }

			// text_region segment_region = {
			// 	i - res.length, res.length,
			// };

			// bool should_draw = regions_overlap(update_regions[current_update_region],
			// 								   segment_region);

			bool should_draw = true;

			draw_string(ctx, &sctx, res, *f, should_draw);

			if (!discard) {
				res.text = &str.text[i];
				res.length = 0;
			} else {
				res.text = &str.text[i+nbytes];
				res.length = 0;
			}

		}

		if (c == '\t') {
			// text_region segment_region = {
			// 	i, 1,
			// };

			// bool should_draw = regions_overlap(update_regions[current_update_region],
			// 								   segment_region);
			string spaces = CSTR_TO_STR("    ");
			bool should_draw = true;

			face *f = 0;

			// while (current_face_segment < syntax.segments.size() &&
			// 	   ch >= face_segment_begin + syntax.segments[current_face_segment].length) {
			// 	face_segment_begin += syntax.segments[current_face_segment].length;
			// 	current_face_segment += 1;
			// }

			// if (current_face_segment < syntax.segments.size()) {
			// 	f = syntax.segments[current_face_segment].face;
			// }

			if (!f) {
				f = syntax.default_face;
			}

			// while (current_update_region < update_regions.size() &&
			// 	   (!value_in_range(i ,
			// 						update_regions[current_update_region].begin,
			// 						update_regions[current_update_region].begin + update_regions[current_update_region].length) ||
			// 		i < update_regions[current_update_region].begin)) {
			// 	current_update_region += 1;
			// }


			draw_string(ctx, &sctx, spaces, *f, should_draw);
		}

		if (!discard) {
			res.length += nbytes;
		}

		if (sctx.y > window->bounds.height) {
			// We have passed the end
			break;
		}

		if (linebreak) {
			sctx.x = 0;
			sctx.y += sctx.max_height_in_line;
			sctx.max_height_in_line = 0;
			line += 1;
		}

		i += nbytes;
		ch += 1;
	}

	if (res.length > 0) {
			// while (current_face_segment < syntax.segments.size() &&
			// 	   ch - 1 >= face_segment_begin + syntax.segments[current_face_segment].length) {
			// 	face_segment_begin += syntax.segments[current_face_segment].length;
			// 	current_face_segment += 1;
			// }

			face *f = 0;

			// if (current_face_segment < syntax.segments.size()) {
			// 	f = syntax.segments[current_face_segment].face;
			// }

			if (!f) {
				f = syntax.default_face;
			}

			// while (current_update_region < update_regions.size() &&
			// 	   (!value_in_range(i - 1,
			// 						update_regions[current_update_region].begin,
			// 						update_regions[current_update_region].begin + update_regions[current_update_region].length) ||
			// 		i - 1 < update_regions[current_update_region].begin)) {
			// 	current_update_region += 1;
			// }

			// if (current_update_region < update_regions.size()) {
			// 	text_region segment_region = {
			// 		i - res.length, res.length,
			// 	};

			bool should_draw = true;

			// 	bool should_draw = regions_overlap(update_regions[current_update_region],
			// 									segment_region);

				draw_string(ctx, &sctx, res, *f, should_draw);
			// }

		// while (current_face_segment < syntax.segments.size() &&
		// 	   ch - 1 >= face_segment_begin + syntax.segments[current_face_segment].length) {
		// 	face_segment_begin += syntax.segments[current_face_segment].length;
		// 	current_face_segment += 1;
		// }

		// face *f = 0;

		// if (current_face_segment < syntax.segments.size()) {
		// 	f = syntax.segments[current_face_segment].face;
		// }

		// if (!f) {
		// 	f = syntax.default_face;
		// }

		// draw_string(ctx, &sctx, res, *f, true);
	}
	cairo_restore(ctx->cairo);

	window->last_change_updated = window->buffer->last_change_id;
}

void paint_window(struct cairo_draw_context *ctx, frame *frame, size_t window_id) {
	size_t child_left = 2 * window_id + 1;
	size_t child_right = child_left + 1;

	bool has_children = false;

	window *wnd = &frame->window_heap[window_id];

	// if (!wnd->dirty) {
	// 	return;
	// }

	if (child_right < frame->window_heap.size()) {
		paint_window(ctx, frame, child_left);
		has_children = true;
	}

	if (child_right < frame->window_heap.size()) {
		paint_window(ctx, frame, child_right);
		has_children = true;
	}

	if (!has_children) {
		if (frame->window_heap[window_id].buffer) {
			paint_window_leaf(ctx, &frame->window_heap[window_id]);
		}
	}

	wnd->dirty = false;
}

void expose_frame(struct cairo_draw_context *ctx, struct frame *frame) {
	// cairo_set_source_rgb(ctx->cairo, 0.0, 0.0, 0.0);
	// cairo_paint(ctx->cairo);
	paint_window(ctx, frame, 0);
}

void mark_dirty_region(frame *frame, size_t window_id, rectangle region) {
	size_t child_left = 2 * window_id + 1;
	size_t child_right = child_left + 1;

	window *wnd = &frame->window_heap[window_id];

	if (!intersects(region, wnd->bounds)) {
		return;
	}

	wnd->dirty = true;

	if (child_right < frame->window_heap.size()) {
		mark_dirty_region(frame, child_left, region);
	}

	if (child_right < frame->window_heap.size()) {
		mark_dirty_region(frame, child_right, region);
	}
}


void expose_damaged_frame(struct cairo_draw_context *ctx, struct frame *frame, rectangle region) {
	mark_dirty_region(frame, 0, region);
	expose_frame(ctx, frame);
}
