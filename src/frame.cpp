enum class frame_frontend {
	X11 = 1,
};

struct frame {
	size_t id;
	frame_frontend frontend;
	std::vector<window> window_heap;
	size_t selected_window;
	size_t width, height;
	int socket;

	void update_window_geometry(size_t window_id) {
		size_t child_left = 2 * window_id + 1;
		size_t child_right = child_left + 1;

		window *wnd = &window_heap[window_id];

		if (wnd->buffer) {
			// This is a leaf node. We are done with this branch.

			return;
		}

		if (child_right < window_heap.size()) {
			window *wnd_left = &window_heap[child_left];

			if (wnd->split_orientation == orientation::horizontal) {
				wnd_left->bounds.x = wnd->bounds.x;
				wnd_left->bounds.y = wnd->bounds.y;
				wnd_left->bounds.width = wnd->bounds.width;
				wnd_left->bounds.height = wnd->bounds.height * wnd->split_ratio;
			} else {
				wnd_left->bounds.x = wnd->bounds.x;
				wnd_left->bounds.y = wnd->bounds.y;
				wnd_left->bounds.width = wnd->bounds.width * wnd->split_ratio;
				wnd_left->bounds.height = wnd->bounds.height;
			}
			
			update_window_geometry(child_left);
		}

		if (child_right < window_heap.size()) {
			window *wnd_right = &window_heap[child_right];

			if (wnd->split_orientation == orientation::horizontal) {
				wnd_right->bounds.x = wnd->bounds.x;
				wnd_right->bounds.y = wnd->bounds.y + wnd->bounds.height * wnd->split_ratio;
				wnd_right->bounds.width = wnd->bounds.width;
				wnd_right->bounds.height = wnd->bounds.height * (1-wnd->split_ratio);
			} else {
				wnd_right->bounds.x = wnd->bounds.x + wnd->bounds.width * wnd->split_ratio;
				wnd_right->bounds.y = wnd->bounds.y;
				wnd_right->bounds.width = wnd->bounds.width * (1-wnd->split_ratio);
				wnd_right->bounds.height = wnd->bounds.height;
			}
			
			update_window_geometry(child_right);
		}
	}

	void resize(size_t width, size_t height) {
		this->width = width;
		this->height = height;
		window_heap[0].bounds.width = this->width;
		window_heap[0].bounds.height = this->height;
		update_window_geometry(0);
	}
};

void frame_mark_window_dirty(frame *frame, size_t window_id) {
	frame->window_heap[window_id].dirty = true;
	while (window_id > 0) {
		window_id = std::max<size_t>(window_id - 1, 0) / 2;
		frame->window_heap[window_id].dirty = true;
	}
}

size_t find_col(string str, size_t position) {
	size_t col = 0;
	while (position > 0) {
		int32_t c = str.text[--position];

		if (c == '\n') {
			break;
		}

		if (c == '\t') {
			col += 4;
		} else {
			col += 1;
		}
	}
	return col;
}

size_t find_line_begin(string str, size_t position) {
	while (position > 0) {
		int32_t c = str.text[--position];

		if (c == '\n') {
			return position + 1;
		}
	}
	return position;
}

size_t move_to_col_in_line(string str, size_t position, size_t col) {
	position = find_line_begin(str, position);

	if (str.text[position] == '\n') {
		return position;
	}

	while (col > 0) {
		int32_t c = str.text[position];

		if (c == '\n') {
			position -= 1;
			break;
		}

		if (c == '\t') {
			if (col >= 4) {
				col -= 4;
			} else {
				col = 0;
			}
		} else {
			col -= 1;
		}

		position += 1;
	}

	return position;
}

size_t find_previous_line_begin(string str, size_t position) {
	position = find_line_begin(str, position);
	position = find_line_begin(str, std::max<size_t>(position, 1) - 1);
	return position;
}

size_t find_next_line_begin(string str, size_t position) {
	while (position < str.length) {
		int32_t c = str.text[position++];

		if (c == '\n') {
			return position;
		}
	}
	return str.length;
}

void notify_cursor_move(frame *frame, size_t window_id, size_t from, size_t to) {
	window *win = &frame->window_heap[window_id];

	text_region region;
	region.length = 1;

	if (from != to) {
		region.begin = from;
		win->buffer->notify_change(region);

		region.begin = to;
		win->buffer->notify_change(region);
	}
}

void window_move_cursor_down(frame *frame, size_t window_id) {
	window *win = &frame->window_heap[window_id];

	if (!win->buffer) {
		return;
	}

	string str = win->buffer->text;
	size_t col = win->cursor_col;
	size_t new_line_base = find_next_line_begin(str, win->cursor);
	size_t new_cur = move_to_col_in_line(str, new_line_base, col);

	notify_cursor_move(frame, window_id, win->cursor, new_cur);

	win->cursor = new_cur;
	frame_mark_window_dirty(frame, window_id);
}

void window_move_cursor_up(frame *frame, size_t window_id) {
	window *win = &frame->window_heap[window_id];

	if (!win->buffer) {
		return;
	}

	string str = win->buffer->text;

	size_t col = win->cursor_col;
	size_t new_line_base = find_previous_line_begin(win->buffer->text, win->cursor);
	size_t new_cur = move_to_col_in_line(str, new_line_base, col);

	notify_cursor_move(frame, window_id, win->cursor, new_cur);

	win->cursor = new_cur;
	frame_mark_window_dirty(frame, window_id);
}

void window_move_cursor_left(frame *frame, size_t window_id) {
	window *win = &frame->window_heap[window_id];

	if (!win->buffer) {
		return;
	}

	string str = win->buffer->text;
	size_t new_cur = win->cursor;

	if (new_cur == 0) {
		return;
	}

	new_cur -= 1;

	// Do not move past beginning of line
	if (str.text[new_cur] == '\n') {
		return;
	}

	notify_cursor_move(frame, window_id, win->cursor, new_cur);

	win->cursor = new_cur;
	win->cursor_col = find_col(str, new_cur);

	frame_mark_window_dirty(frame, window_id);
}

void window_move_cursor_right(frame *frame, size_t window_id) {
	window *win = &frame->window_heap[window_id];

	if (!win->buffer) {
		return;
	}

	string str = win->buffer->text;
	size_t new_cur = win->cursor;

	if (new_cur >= str.length) {
		new_cur = str.length;
		win->cursor = new_cur;
	} else {
		new_cur += 1;

		// Do not move past end of line
		if (str.text[win->cursor] == '\n' || str.text[new_cur] == '\n') {
			return;
		}
	}

	notify_cursor_move(frame, window_id, win->cursor, new_cur);

	win->cursor = new_cur;
	win->cursor_col = find_col(str, new_cur);

	frame_mark_window_dirty(frame, window_id);
}
