// TODO: Make multithreaded?
template<typename T, size_t N>
struct circular_buffer {
	std::array<T, N> data;
	size_t head;
	size_t tail;
	size_t count;

	circular_buffer()
		: head(0), tail(0), count(0) {}

	void push(const T &val) {
		data[head] = val;
		count = std::min(count + 1, N);
		head = (head+1) % N;
		if (tail == head) {
			tail = (tail+1) % N;
		}
	}

	optional<T> pop() {
		if (count > 0) {
			optional<T> res = optional<T>(data[tail]);
			count -= 1;
			tail = (tail+1) % N;
			return res;
		}
		return optional<T>();
	}

	optional<T> first() const {
		return (*this)[0];
	}

	optional<T> last() const {
		return (*this)[count-1];
	}

	optional<T> operator[](size_t i) const {
		if (i >= count) {
			return optional<T>();
		}
		return optional<T>(data[(tail + (count - i) - 2) % N]);
	}

	size_t size() const {
		return count;
	}
};
