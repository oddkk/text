struct linked_list_buffer {
	paged_arena storage;

	struct segment {
		string data;
		paged_arena::arena_page *page;
	};
	std::list<segment> segments;
	size_t length;

	using segment_iterator = std::list<segment>::iterator;

	std::pair<segment_iterator, size_t> find_segment(size_t pos) {
		size_t segment_begin = 0;
		for (auto it = segments.begin(); it != segments.end(); it++) {
			if (pos >= segment_begin && pos < segment_begin + it->data.length) {
				return make_pair(it, segment_begin);
			}
			segment_begin += it->data.length;
		}
		return make_pair(segments.end(), length);
	}

	segment_iterator split_segment(segment_iterator seg, size_t pos) {
		assert(pos < seg->data.length);
		assert(seg != segments.end());

		size_t first_segment_length = pos;
		size_t second_segment_length = seg->data.length - pos;

		if (first_segment_length > 0) {
			segment new_segment = *seg;
			new_segment.data.text = &seg->data.text[first_segment_length];
			new_segment.data.length = second_segment_length;
			new_segment.page->users += 1;

			seg->data.length = first_segment_length;

			seg++;
			seg = segments.insert(seg, new_segment);
		}

		return seg;
	}

	void insert(size_t pos, string str) {
		auto seg_res = find_segment(pos);
		segment_iterator seg = seg_res.first;
		size_t seg_begin = seg_res.second;

		if (seg != segments.end()) {
			seg = split_segment(seg, pos - seg_begin);
		}

		size_t alloced = 0;

		while (alloced < str.length) {
			segment new_segment = {};
			auto alloc_result = storage.alloc_part_str(str.length - alloced);

			new_segment.page = alloc_result.first;
			new_segment.data = alloc_result.second;

			copy_memory(new_segment.data.text, &str.text[alloced], new_segment.data.length);
			alloced += new_segment.data.length;

			segments.insert(seg, new_segment);
		}

		this->length += str.length;
	}

	void remove(size_t begin, size_t length) {
		auto seg_res = find_segment(begin);
		segment_iterator seg = seg_res.first;
		size_t seg_begin = seg_res.second;
		size_t end = begin + length;

		while (seg != segments.end() && seg_begin < end) {
			size_t old_length = seg->data.length;
			size_t seg_end = seg_begin + old_length;
			bool incremented = false;
			if (begin > seg_begin) {
				assert(begin < seg_end);

				split_segment(seg, begin - seg_begin);
			} else {
				if (end < seg_end) {
					size_t new_length = seg_end - end;
					size_t removed_from_front = old_length - new_length;
					seg->data.text = &seg->data.text[removed_from_front];
					seg->data.length = new_length;
				} else {
					storage.remove_page_user(seg->page);
					seg = segments.erase(seg);
					incremented = true;
				}
			}

			seg_begin += old_length;
			if (!incremented)
				seg++;
		}

		this->length -= length;
	}

	void print() {
		for (segment &seg : segments) {
			printf("%.*s", LIT(seg.data));
		}
		printf("\n");
	}
};
