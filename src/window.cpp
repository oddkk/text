enum class orientation {
	horizontal,
	vertical
};

struct window {
	struct buffer *buffer;
	rectangle bounds;
	size_t cursor;
	size_t cursor_col;
	size_t scroll;
	orientation split_orientation;
	double split_ratio;
	size_t last_change_updated;
	bool dirty;
};
