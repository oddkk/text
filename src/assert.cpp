bool _assert(bool condition, const char *msg, const char *cond_str, const char *file, const char *func, size_t line) {
	if (!condition) {
		fprintf(stderr, "Assertion failed: %s\n", cond_str);
		if (msg) {
			fprintf(stderr, "%s\n", msg);
		}
		fprintf(stderr, "%s(%lu):%s\n", file, line, func);
		abort();
	}
	return condition;
}

#define assertm(cond, msg) _assert(cond, msg, #cond, __FILE__, __func__, __LINE__)
#define assert(cond) _assert(cond, NULL, #cond, __FILE__, __func__, __LINE__)
