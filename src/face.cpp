enum text_decoration {
	TEXT_DECORATION_UNDERLINE = 1,
	TEXT_DECORATION_STRIKETHROUGH = 2
};

struct face {
	enum text_decoration decoration;
	struct color foreground_color;
	struct color background_color;
	struct string family;
	double size;
};
