template<typename T>
struct optional {
	using storage_t = typename std::aligned_storage<sizeof(T)>::type;

	bool set;
	storage_t storage;

	optional(T &&dt) : set(true) {
		new(&storage) T(std::forward<T>(dt));
	}

	optional(const T &dt) : set(true) {
		new(&storage) T(dt);
	}

	optional() : set(false) {}

	const T &data() const {
		return *reinterpret_cast<const T*>(&storage);
	}

	operator bool() const {
		return set;
	}

	template<typename Func>
	auto bind(Func func) -> decltype(func(this->data())) {
		if (set) {
			return func(data());
		} else {
			return decltype(func(this->data()))();
		}
	}

	~optional() {
		if (set) {
			reinterpret_cast<T*>(&storage)->~T();
		}
	}
};
