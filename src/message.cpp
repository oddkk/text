namespace messaging {
	constexpr size_t max_message_size = 1024;

	enum class message_type {
		create_frame_request,
		create_frame_response,
		destroy_frame,
		buffer_request,
		buffer_response,
		buffer_changed,
		geometry_changed,
		keypress,
	};

	string message_type_name[] = {
		CSTR_TO_STR("create_frame_request"),
		CSTR_TO_STR("create_frame_response"),
		CSTR_TO_STR("destroy_frame"),
		CSTR_TO_STR("buffer_request"),
		CSTR_TO_STR("buffer_response"),
		CSTR_TO_STR("buffer_changed"),
		CSTR_TO_STR("geometry_changed"),
		CSTR_TO_STR("keypress"),
	};

	struct message_create_frame_request {
		frame_frontend frontend;
	};

	struct message_create_frame_response {
		uint64_t frame_id;
	};

	struct message_destroy_frame {
		uint64_t frame_id;
	};

	struct message_buffer_request {
	};

	struct message_buffer_response {
	};

	struct message_buffer_changed {
	};

	struct message_geometry_changed {
		uint32_t new_width;
		uint32_t new_height;
	};

	struct message_keypress {
		uint32_t keysym;
	};

	struct message {
		message_type type;

		union {
			message_create_frame_request create_frame_request;
			message_create_frame_response create_frame_response;
			message_destroy_frame destroy_frame;
			message_buffer_request buffer_request;
			message_buffer_response buffer_response;
			message_buffer_changed buffer_changed;
			message_geometry_changed geometry_changed;
			message_keypress keypress;
		};
	};

	template<size_t N>
	void serialize_message(write_buffer<N> *buffer, message msg) {
		buffer->write_uint32((uint32_t)msg.type);

		switch (msg.type) {
		case message_type::create_frame_request:
			buffer->write_uint32((uint32_t)msg.create_frame_request.frontend);
			break;

		case message_type::create_frame_response:
			buffer->write_uint64((uint64_t)msg.create_frame_response.frame_id);
			break;

		case message_type::destroy_frame:
			buffer->write_uint64((uint64_t)msg.destroy_frame.frame_id);
			break;

		case message_type::keypress:
			buffer->write_uint32((uint32_t)msg.keypress.keysym);
			break;

		case message_type::geometry_changed:
			buffer->write_uint32(msg.geometry_changed.new_width);
			buffer->write_uint32(msg.geometry_changed.new_height);
			break;

		default:
			break;
		}
	}

	struct no_errors {
		bool error;

		no_errors() : error(false) {}

		template<typename T>
		T get(optional<T> val, T def=0) {
			if (val) {
				return val.data();
			} else {
				error = true;
				return def;
			}
		}
	};

	template<size_t N>
	optional<message> deserialize_message(read_buffer<N> *buffer) {
		message msg = {};
		no_errors ne;

		msg.type = (messaging::message_type)ne.get(buffer->read_uint32());

		switch (msg.type) {
		case message_type::create_frame_request:
			msg.create_frame_request.frontend = (frame_frontend)ne.get(buffer->read_uint32());
			break;

		case message_type::create_frame_response:
			msg.create_frame_response.frame_id = ne.get(buffer->read_uint64());
			break;

		case message_type::destroy_frame:
			msg.destroy_frame.frame_id = ne.get(buffer->read_uint64());
			break;

		case message_type::keypress:
			msg.keypress.keysym = ne.get(buffer->read_uint32());
			break;

		case message_type::geometry_changed:
			msg.geometry_changed.new_width = ne.get(buffer->read_uint32());
			msg.geometry_changed.new_height = ne.get(buffer->read_uint32());
			break;

		default:
			break;
		}

		if (!ne.error) {
			return optional<message>(msg);
		} else {
			return optional<message>();
		}
	}

	struct messages {
		static constexpr size_t max_message_size = 1024;
		using message_size_t = uint32_t;

		read_buffer<max_message_size> read_buffer;
		write_buffer<max_message_size> write_buffer;
		int socket;

		size_t bytes_read;
		bool size_read;

		bool recv_bytes(uint8_t *buffer, size_t length) {
			size_t bytes_read = 0;
			while (bytes_read < length) {
				ssize_t ret = recv(socket, buffer + bytes_read,
								   length - bytes_read, 0);
				if (ret == -1) {
					perror("recv");
					return false;
				}
				bytes_read += ret;
			}
			return true;
		}

		optional<message> try_receive() {
			int bytes_available;
			if (ioctl(socket, FIONREAD, &bytes_available) == -1) {
				perror("ioctl");
				return optional<message>();
			}
			if (!size_read) {
				if ((size_t)bytes_available < sizeof(message_size_t)) {
					return optional<message>();
				}

				message_size_t normal_message_size;
				recv_bytes((uint8_t*)&normal_message_size, sizeof(message_size_t));

				read_buffer.read_position = 0;
				read_buffer.size = big_endian_to_host<message_size_t>(normal_message_size);
				size_read = true;
				bytes_read = 0;

				if (read_buffer.size > max_message_size) {
					fprintf(stderr, "received too big message (%lu, max: %lu)\n", read_buffer.size, max_message_size);
				}
			}
 
			if (ioctl(socket, FIONREAD, &bytes_available) == -1) {
				perror("ioctl");
				return optional<message>();
			}

			size_t to_read = std::min<size_t>(read_buffer.size, bytes_read + bytes_available);
			
			recv_bytes(&read_buffer.buffer[bytes_read], to_read);
			bytes_read += to_read;

			if (bytes_read >= read_buffer.size) {
				read_buffer.read_position = 0;
				auto res = deserialize_message(&read_buffer);
				if (!res) {
					printf("got invalid message\n");
				}
				size_read = false;
				bytes_read = 0;
				read_buffer.reset();
				return res;
			}
			
			return optional<message>();
		}

		void send_bytes(uint8_t *data, size_t length) {
			size_t bytes_sent = 0;
			while (bytes_sent < length) {
				ssize_t ret = ::send(socket, data + bytes_sent,
									 length-bytes_sent, 0);
				if (ret == -1) {
					perror("send");
					return;
				}
				bytes_sent += ret;
			}
		}

		void send(message msg) {
			write_buffer.reset();
			serialize_message(&write_buffer, msg);

			message_size_t message_size = write_buffer.write_position;
			message_size_t normal_message_size = host_to_big_endian<message_size_t>(message_size);
			send_bytes((uint8_t*)&normal_message_size, sizeof(message_size_t));
			send_bytes(write_buffer.buffer, message_size);
		}
	};
}
