template<typename T>
T host_to_big_endian(T);
template<> uint64_t host_to_big_endian(uint64_t val) { return htobe64(val); }
template<> uint32_t host_to_big_endian(uint32_t val) { return htobe32(val); }
template<> uint16_t host_to_big_endian(uint16_t val) { return htobe16(val); }
template<> uint8_t  host_to_big_endian(uint8_t  val) { return val; }

template<typename T>
T big_endian_to_host(T);
template<> uint64_t big_endian_to_host(uint64_t val) { return be64toh(val); }
template<> uint32_t big_endian_to_host(uint32_t val) { return be32toh(val); }
template<> uint16_t big_endian_to_host(uint16_t val) { return be16toh(val); }
template<> uint8_t  big_endian_to_host(uint8_t  val) { return val; }

template<size_t N>
struct write_buffer {
	uint8_t buffer[N];
	uint64_t write_position;

	void write_data(uint8_t *dt, size_t n) {
		copy_memory(&buffer[write_position], dt, n);
		write_position += n;
	}

	template<typename T>
	void write_integer(T val) {
		val = host_to_big_endian<T>(val);
		write_data((uint8_t*)&val, sizeof(val));
	}

	void write_uint64(uint64_t val) {write_integer<uint64_t>(val);}
	void write_uint32(uint32_t val) {write_integer<uint32_t>(val);}
	void write_uint16(uint16_t val) {write_integer<uint16_t>(val);}
	void write_uint8(uint8_t val)   {write_integer<uint8_t>(val);}

	void reset() {
		write_position = 0;
	}
};

template<typename T>
optional<T> data_to_integer(const std::array<uint8_t, sizeof(T)> dt) {
	T res = *reinterpret_cast<const T*>(dt.data());
	return optional<T>(res);
}

template<size_t N>
struct read_buffer {
	uint8_t buffer[N];
	uint64_t read_position;
	uint64_t size;

	template<size_t M>
	optional<std::array<uint8_t, M>> read_bytes() {
		if (read_position + M > size) {
			printf("Not enough data (%lu, %lu, %lu)\n", size, M, read_position);
			return optional<std::array<uint8_t, M>>();
		}
		std::array<uint8_t, M> res;
		copy_memory(res.data(), &buffer[read_position], M);
		read_position += M;
		return optional<std::array<uint8_t, M>>(res);
	}

	template<typename T>
	optional<T> read_integer() {
		return read_bytes<sizeof(T)>()
			.bind(data_to_integer<T>)
			.bind([](T val){ return optional<T>(big_endian_to_host(val)); });
	}

	optional<uint64_t> read_uint64() {return read_integer<uint64_t>();}
	optional<uint32_t> read_uint32() {return read_integer<uint32_t>();}
	optional<uint16_t> read_uint16() {return read_integer<uint16_t>();}
	optional<uint8_t>  read_uint8()  {return read_integer<uint8_t>();}

	void reset() {
		read_position = 0;
		size = 0;
	}
};

