struct face_segment_context {
	std::vector<face_segment> segments;
	face *default_face;
	face cursor_face;

	face_segment_context() = default;

	face_segment_context(face_segment *segments, size_t num_segments, size_t cursor, face *default_face)
		: default_face(default_face) {
		this->segments.reserve(num_segments + 1);

		face_segment cursor_segment;

		cursor_segment.length = 1;
		cursor_segment.face = &this->cursor_face;

		size_t pos = 0;
		for (size_t i = 0; i < num_segments; ++i) {
			face_segment *segment = &segments[i];

			size_t begin = pos;
			size_t end = pos + segment->length;

			if (cursor >= begin && cursor < end) {
				size_t first_segment_length = cursor - begin;
				if (first_segment_length > 0) {
					face_segment first_face;
					first_face.length = first_segment_length;
					first_face.face = segment->face;
					this->segments.push_back(first_face);
				}

				{
					if (segment->face) {
						create_cursor_face(segment->face);
					} else {
						create_cursor_face(this->default_face);
					}

					this->segments.push_back(cursor_segment);
				}

				size_t last_segment_length = end - cursor;
				if (last_segment_length > 1) {
					face_segment last_face;
					last_face.length = last_segment_length - 1;
					last_face.face = segment->face;
					this->segments.push_back(last_face);
				}

			} else {
				this->segments.push_back(*segment);
			}

			pos += segment->length;
		}

		if (pos <= cursor) {
			face_segment pre_cursor_segment;

			pre_cursor_segment.length = cursor - pos;
			pre_cursor_segment.face = 0;

			this->segments.push_back(pre_cursor_segment);

			create_cursor_face(this->default_face);

			this->segments.push_back(cursor_segment);
		}
	}

	void create_cursor_face(face *face) {
		this->cursor_face = *face;
		this->cursor_face.foreground_color = face->background_color;
		this->cursor_face.background_color = face->foreground_color;
	}
};
