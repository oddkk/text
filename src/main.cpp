#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cairo/cairo.h>
#include <cairo/cairo-xlib.h>

#include <vector>
#include <array>
#include <list>
#include <algorithm>
#include <functional>

#include <cinttypes>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#define FRAME_X11

#include "assert.cpp"
#include "memory.cpp"
#include "optional.cpp"
#include "circular_buffer.cpp"
#include "color.cpp"
#include "rectangle.cpp"
#include "string.cpp"
#include "palette.cpp"
//#include "file_buffer.cpp"
#include "paged_arena.cpp"
#include "linked_list_buffer.cpp"
#include "face.cpp"
#include "buffer.cpp"
#include "face_segment.cpp"
#include "window.cpp"
#include "frame.cpp"
#include "socket_buffer.cpp"
#include "message.cpp"

#include "draw_cairo.cpp"

enum class program_mode {
	client = 1,
	server = 2,
	standalone = 3,
};

#include "server.cpp"

#ifdef FRAME_X11
#include "frame_x11.cpp"
#endif

struct string read_entire_file(const char *file) {
	struct string result = {};
	FILE *fp = fopen(file, "rb");

	if (!fp) {
		return string{};
	}

	fseek(fp, 0, SEEK_END);
	result.length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	result.text = (uint8_t*)malloc(result.length);

	fread(result.text, 1, result.length, fp);

	fclose(fp);

	return result;
}

struct string read_entire_file_into_buffer(const char *file, linked_list_buffer *buffer) {
	struct string result = {};
	FILE *fp = fopen(file, "rb");

	if (!fp) {
		return string{};
	}

	size_t bytes_read = 0;
	uint8_t bfr[paged_arena::arena_page_size];
	string str;
	str.length = 0;
	str.text = bfr;

	while (true) {
		ssize_t res = fread(bfr, sizeof(uint8_t), sizeof(bfr), fp);

		if (res <= 0) {
			if (feof(fp)) {
				break;
			} else {
				perror("read");
				break;
			}
		}
		str.length = res;
		buffer->insert(bytes_read, str);

		bytes_read += res;
	}

	fclose(fp);

	return result;
}

void client(int socket) {
	frame frame = {};
	buffer loading_buffer = {};
	window wnd = {};
	face default_face = {};

	loading_buffer.text = CSTR_TO_STR("Loading... \u001b\u0000\u0001Test\u001b\u0000\u0000\n");

	default_face.family = CSTR_TO_STR("SourceCodePro");
	default_face.size = 12;
	default_face.foreground_color = rgb(255, 255, 255);
	default_face.background_color = rgb(0, 0, 0);

	loading_buffer.default_face = &default_face;

	struct face_segment face_segments[] = {
		{0, NULL},
	};

	loading_buffer.face_segments = face_segments;
	loading_buffer.num_face_segments = 0;

	wnd.buffer = &loading_buffer;
	frame.window_heap.push_back(wnd);

	frame.socket = socket;

	#ifdef FRAME_X11
	frame_x11(&frame);
	#endif
}

int main() {
	program_mode mode = program_mode::standalone;

	if (mode == program_mode::standalone) {
		int sv[2];

		if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv) == -1) {
			perror("socketpair");
			exit(1);
		}

		int pid = fork();

		if (pid == -1) {
			perror("fork");
			exit(1);
		} else if (pid == 0) {
			// We are the child
			client(sv[1]);
		} else {
			// We are the parent
			server(sv[0], mode);
			wait(NULL);
		}
	} else if (mode == program_mode::client) {
		// open socket
		client(0);
	} else if (mode == program_mode::server) {
		// open socket
		server(0, mode);
	}
}
