struct server_context {
	messaging::messages messages;
	program_mode mode;
	std::list<frame> frames;
	size_t next_frame_id;
	bool should_close;
};

void server_dispatch_message_create_frame_request(server_context *srv, messaging::message msg) {
	frame new_frame = {};
	new_frame.id = ++srv->next_frame_id;
	new_frame.frontend = msg.create_frame_request.frontend;
	srv->frames.push_back(new_frame);

	messaging::message response;
	response.type = messaging::message_type::create_frame_response;
	response.create_frame_response.frame_id = new_frame.id;
	srv->messages.send(response);

}

void server_dispatch_message_destroy_frame(server_context *srv, messaging::message msg) {
	auto iter = srv->frames.begin();
	size_t target_frame_id = msg.destroy_frame.frame_id;
	for (; iter != srv->frames.end(); iter++) {
		if (iter->id == target_frame_id) {
			break;
		}
	}
	if (iter == srv->frames.end()) {
		fprintf(stderr, "Tried to destroy nonexisting frame with id %lu\n", target_frame_id);
		return;
	} else {
		srv->frames.erase(iter);
	}

	if (srv->mode == program_mode::standalone && srv->frames.empty()) {
		srv->should_close = true;
	}
}

void server_dispatch_message_keypress(server_context *srv, messaging::message msg) {
	printf("Received keypress %i\n", msg.keypress.keysym);
}

void server_dispatch_message(server_context *srv, messaging::message msg) {
	switch (msg.type) {
	case messaging::message_type::create_frame_request:
		server_dispatch_message_create_frame_request(srv, msg);
	break;

	case messaging::message_type::destroy_frame:
		server_dispatch_message_destroy_frame(srv, msg);
	break;

	case messaging::message_type::keypress:
		server_dispatch_message_keypress(srv, msg);
	break;

	default: {
		fprintf(stderr, "server: Got unexpected message with id %i", (int)msg.type);
		if ((size_t)msg.type < sizeof(messaging::message_type_name) / sizeof(string)) {
			fprintf(stderr, " (%.*s)", LIT(messaging::message_type_name[(int)msg.type]));
		}
		fprintf(stderr, "\n");
	} break;
	}
}

void server(int socket, program_mode mode) {
	fd_set in_fds;
	server_context context = {};

	context.mode = mode;
	context.messages.socket = socket;

	while (!context.should_close) {
		int fd;
		FD_ZERO(&in_fds);
		FD_SET(context.messages.socket, &in_fds);

		fd = select(context.messages.socket+1, &in_fds, 0, 0, 0);

		context.messages.try_receive().bind([&context](messaging::message msg) {
				server_dispatch_message(&context, msg);
			});
	}
}
