struct face_segment {
	size_t length;
	struct face *face;
};

struct text_region {
	size_t begin;
	size_t length;
};

struct text_change_region {
	size_t change_id;
	text_region region;
};

struct buffer {
	struct string text;
	struct face_segment *face_segments;
	struct face *default_face;
	size_t num_face_segments;

	static constexpr size_t max_changed_regions = 10;
	typedef std::array<text_region, max_changed_regions> change_regions_t;
	circular_buffer<text_change_region, max_changed_regions> changed_regions;
	size_t last_change_id;

	void notify_change(text_region region) {
		text_change_region change;
		change.region = region;
		change.change_id = ++last_change_id;
		changed_regions.push(change);
	}

	bool should_update_all(size_t last_update_id) {
		auto last_recorded_change = changed_regions.last();

		if (last_update_id == 0) {
			return true;
		}

		if (last_recorded_change.set) {
			return (last_update_id < last_recorded_change.data().change_id);
		}

		return false;
	}

	// The returned regions are guaranteed to be ascending.
	void regions_to_update(size_t last_update_id,
						   std::array<text_region, max_changed_regions> *regions,
						   size_t *num_regions) {
		if (should_update_all(last_update_id)) {
			(*regions)[0] = text_region{0, text.length};
			*num_regions = 1;
			return;
		}

		*num_regions = 0;

		// Copy all regions after last update into output.
		for (size_t i = 0; i < changed_regions.size(); ++i) {
			auto change = changed_regions[i];
			if (!change.set) {
				break;
			}

			if (change.data().change_id > last_update_id) {
				(*regions)[*num_regions] = change.data().region;
				*num_regions += 1;
			}
		}

		// Aggregate regions to reduce number of actual updates.
		std::sort(regions->begin(), regions->begin() + *num_regions,
				  [](text_region &lhs, text_region &rhs) {
					  return lhs.begin < rhs.begin;
				  });

		// size_t wr = 0;
		// printf("len %lu\n", *num_regions);
		// for (size_t i = 0; i < *num_regions; ++i) {
		// 	text_region *iter = &(*regions)[i];
		// 	text_region *write = &(*regions)[wr];

		// 	size_t iter_end = iter->begin + iter->length;
		// 	size_t write_end = write->begin + write->length;
			
		// 	printf("iter %lu %lu\n", iter_end, write_end);

		// 	if (iter->begin <= write_end) {
		// 		write->length = std::max<size_t>(write_end - iter_end, write->length);
		// 	} else {
		// 		wr += 1;
		// 	}
		// }

		// *num_regions = wr + 1;
	}
};

