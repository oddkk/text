struct rectangle {
	int x, y, width, height;
};

template<typename T>
bool value_in_range(const T value, const T min, const T max) {
	return (value >= min) && (value <= max);
}

bool intersects(const rectangle lhs, rectangle rhs) {
	int lhs_left = lhs.x,
		lhs_right = lhs.x + lhs.width,
	    lhs_top = lhs.y,
		lhs_bottom = lhs.y + lhs.height,

	    rhs_left = rhs.x,
		rhs_right = rhs.x + rhs.width,
	    rhs_top = rhs.y,
		rhs_bottom = rhs.y + rhs.height;

	bool x_overlap =
		value_in_range(lhs_left, rhs_left, rhs_right) ||
		value_in_range(rhs_left, lhs_left, lhs_right);

	bool y_overlap =
		value_in_range(lhs_top, rhs_top, rhs_bottom) ||
		value_in_range(rhs_top, lhs_top, lhs_bottom);

	return x_overlap && y_overlap;
}
