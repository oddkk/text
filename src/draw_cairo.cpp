struct cairo_draw_context {
	cairo_t *cairo;
	uint32_t palette_index;
};

void parse_escape_code(cairo_draw_context *ctx, string text, uint8_t **it) {
}

void expose_frame(cairo_draw_context *ctx, frame *frame) {
}

void expose_damaged_frame(cairo_draw_context *ctx, frame *frame, rectangle region) {
	expose_frame(ctx, frame);
}
