#!/bin/bash

CC=clang++
VERSION=0.1

FT_CFLAGS=$(freetype-config --cflags)
FT_LDFLAGS=$(freetype-config --libs)

CFLAGS="-g -std=c++11 -Wall -Wextra -pedantic -Wno-gnu-empty-initializer -Wno-unused-function -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L"
LIBS="-lcairo -lX11 -lpthread $FT_LDFLAGS"
INCLUDE="$FT_CFLAGS"

CFLAGS="$CFLAGS -O0"

$CC $CFLAGS $INCLUDE $LIBS -o text -Isrc/ src/main.cpp
